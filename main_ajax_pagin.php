<?php
// Connect DB
connect();
 
// Phân trang
$config = array(
    'current_page'  => isset($_GET['page']) ? $_GET['page'] : 1,
    'total_record'  => count_all_member(), // tổng số thành viên
    'limit'         => 3,
    'link_full'     => 'index.php?page={page}',
    'link_first'    => 'index.php',
    'range'         => 10
);
 
$paging = new Pagination();
$paging->init($config);
 
// Lấy limit, start
$limit = $paging->get_config('limit');
$start = $paging->get_config('start');
 
// Lấy danh sách thành viên
$member = get_all_member($limit, $start);
 
// Kiểm tra nếu là ajax request thì trả kết quả
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    die (json_encode(array(
        'member' => $member,
        'paging' => $paging->html()
    )));
}
 
// Disconnect DB
disconnect();