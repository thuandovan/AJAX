<?php
// Import thư viện data vào
require_once 'connection.php';
 
// Load thư viện phân trang
include_once 'main_pagin.php';
include_once 'main_ajax_pagin.php';
 
?>
<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
      li{float:left; margin: 3px; border: solid 1px gray; list-style: none}
      a{padding: 5px;}
      span{display:inline-block; padding: 0px 3px; background: blue; color:white }
    </style>
      <script language="javascript" src="http://code.jquery.com/jquery-2.0.0.min.js"></script>
  </head>
  <body>
    <div id="content">
      <div id="list">
        <table border="1" cellspacing="0" cellpadding="5">
          <?php foreach ($member as $item){ ?>
          <tr>
              <td>
                <?php echo $item['ID']; ?>  
              </td>
              <td>
                <?php echo $item['user_name']; ?> 
              </td>
              <td>
                <?php echo $item['user_pass']; ?>  
              </td>
              <td>
                <?php echo $item['user_email']; ?>  
              </td>
              <td>
                <?php echo $item['user_status']; ?>  
              </td>
          </tr>
          <?php } ?>
        </table>
      </div>
      <div id="paging">
        <?php echo $paging->html(); ?>
      </div>
    </div>
    <script language="javascript">
      $('#content').on('click','#paging a', function ()
      {
        var url = $(this).attr('href');
        $.ajax(
        {
          url : url,
          type : 'get',
          dataType : 'json',
          success : function (result)
          {
            //  kiểm tra kết quả đúng định dạng không
            if (result.hasOwnProperty('member') && result.hasOwnProperty('paging'))
            {
              var html = '<table border="1" cellspacing="0" cellpadding="5">';
               // lặp qua danh sách thành viên và tạo html
              $.each(result['member'], function (key, item)
              {
                html += '<tr>';
                html += '<td>'+item['ID']+'</td>'; 
                html += '<td>'+item['user_name']+'</td>';
                html += '<td>'+item['user_pass']+'</td>';
                html += '<td>'+item['user_email']+'</td>';
                html += '<td>'+item['user_status']+'</td>';
                html += '</tr>';
              });
                
              html += '</table>';
                
              // Thay đổi nội dung danh sách thành viên
              $('#list').html(html);
                
              // Thay đổi nội dung phân trang
              $('#paging').html(result['paging']);
                
              // Thay đổi URL trên website
              window.history.pushState({path:url},'',url);
            }
          }
        });
        return false;
      });
    </script>
  </body>
</html>