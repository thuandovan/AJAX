<?php
	// require ('connection.php');
	// $query_users = "SELECT * FROM users limit $start,$number_record";
	// $users = mysqli_query($conn, $query_users);
	// $getList = array();

	class Pagination
	{
		protected $_config = array(
			'current_page' 	=> 1,
			'total_record' 	=> 1,
			'total_page' 	=> 1,
			'limit' 		=> 3,
			'start' 		=> 0,
			'link_full' 	=> '',
			'link_first' 	=> '',
			'range' 		=> 1, // So button trang muon hien thi
			'min' 			=> 0,
			'max' 			=> 0  // Tham so min, max la tham so private
			);
		function get_config($key)
		{
			return $this->_config[$key];
		}

		function init($config = array())
		{
			foreach ($config as $key => $value) {
				if (isset($this->_config[$key]))
				{
					$this->_config[$key] = $value;
				}
			}

			if ($this->_config['limit'] < 0)
			{
				$this->_config['limit'] = 0;
			}

			$this->_config['total_page'] = ceil($this->_config['total_record']/$this->_config['limit']);
			if (!$this->config['total_page'])
			{
				$this->_config['total_page'] = 1;
			}

			if ($this->_config['current_page'] < 1)
			{
				$this->_config['current_page'] = 1;
			}

			if ($this->_config['current_page'] > $this->_config['total_page'])
			{
				$this->config['current_page'] = $this->_config['total_page'];
			}

			$this->_config['start'] = ($this->_config['current_page'] - 1) * $this->_config['limit'];

			$middle = ceil($this->_config['range'] / 2);

			if ($this->_config['total_page'] < $this->_config['range'])
			{
        $this->_config['min'] = 1;
        $this->_config['max'] = $this->_config['total_page'];
      }
      // Trường hợp tổng số trang mà lớn hơn range
      else
      {
        // Ta sẽ gán min = current_page - (middle + 1)
        $this->_config['min'] = $this->_config['current_page'] - ($middle + 1);
         
        // Ta sẽ gán max = current_page + (middle - 1)
        $this->_config['max'] = $this->_config['current_page'] + ($middle - 1);
         
        // Sau khi tính min và max ta sẽ kiểm tra
        // nếu min < 1 thì ta sẽ gán min = 1  và max bằng luôn range
        if ($this->_config['min'] < 1)
        {
            $this->_config['min'] = 1;
            $this->_config['max'] = $this->_config['range'];
        }
         
        // Ngược lại nếu min > tổng số trang
        // ta gán max = tổng số trang và min = (tổng số trang - range) + 1 
        else if ($this->_config['max'] > $this->_config['total_page']) 
        {
          $this->_config['max'] = $this->_config['total_page'];
          $this->_config['min'] = $this->_config['total_page'] - $this->_config['range'] + 1;
        }
      }
		}

		/*
     * Hàm lấy link theo trang
     */
    private function __link($page)
    {
        // Nếu trang < 1 thì ta sẽ lấy link first
        if ($page <= 1 && $this->_config['link_first']){
            return $this->_config['link_first'];
        }
        // Ngược lại ta lấy link_full
        // Như tôi comment ở trên, link full có dạng domain.com/page/{page}.
        // Trong đó {page} là nơi bạn muốn số trang sẽ thay thế vào
        return str_replace('{page}', $page, $this->_config['link_full']);
    }
     
    /*
     * Hàm lấy mã html
     * Hàm này ban tạo giống theo giao diện của bạn
     * tôi không có config nhiều vì rất rối
     * Bạn thay đổi theo giao diện của bạn nhé
     */
    function html()
    {   
      $p = '';
      if ($this->_config['total_record'] > $this->_config['limit'])
      {
        $p = '<ul>';
         
        // Nút prev và first
        if ($this->_config['current_page'] > 1)
        {
          $p .= '<li><a href="'.$this->__link('1').'">First</a></li>';
          $p .= '<li><a href="'.$this->__link($this->_config['current_page']-1).'">Prev</a></li>';
        }
         
        // lặp trong khoảng cách giữa min và max để hiển thị các nút
        for ($i = $this->_config['min']; $i <= $this->_config['max']; $i++)
        {
          // Trang hiện tại
          if ($this->_config['current_page'] == $i)
          {
            $p .= '<li><span>'.$i.'</span></li>';
          }
          else
          {
            $p .= '<li><a href="'.$this->__link($i).'">'.$i.'</a></li>';
          }
        }

        // Nút last và next
        if ($this->_config['current_page'] < $this->_config['total_page'])
        {
          $p .= '<li><a href="'.$this->__link($this->_config['current_page'] + 1).'">Next</a></li>';
          $p .= '<li><a href="'.$this->__link($this->_config['total_page']).'">Last</a></li>';
        }
         
        $p .= '</ul>';
      }
      return $p;
    }
	}
?>
